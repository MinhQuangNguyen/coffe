<?php

namespace App\Http\Requests\Backend\Menu;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class UpdateMenuRequest.
 */
class UpdateMenuRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'max:100'],
            'link' => ['max:255'],
            'parent_id' => ['nullable', 'numeric'],
            'category_id' => ['nullable', 'numeric'],
            'sort' => ['nullable', 'numeric']
        ];
    }
}
