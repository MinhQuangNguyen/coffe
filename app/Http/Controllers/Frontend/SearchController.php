<?php

namespace App\Http\Controllers\Frontend;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\Controller;
use App\Services\SearchService;
use Illuminate\Http\Request;
use App\Services\CategoryService;
/**
 * Class SearchController.
 */
class SearchController extends Controller
{
    /**
     * ProjectController constructor.
     *
     * @param  SearchService  $searchService
     */
    public function __construct(SearchService $searchService, CategoryService $categoryService)
    {
        $this->searchService = $searchService;
        $this->categoryService = $categoryService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('k');
        $page = $request->get('page');
        // Lấy dữ liệu của Thực đơn ( Project )
        $projects = $this->searchService->getProjectByCondition($keyword);
        // Lấy dữ liệu của bài viết ( Blog )
        $posts = $this->searchService->getPostByCondition($keyword);
        $categories = $this->categoryService->getNewCategories();
        $resultLength = count($projects) + count($posts);
       
        return view('frontend.search.result', [
            'projects' => $projects,
            "posts" => $posts,
            'categories' => $categories,
            'keyword' => $keyword,
            'resultLength' => $resultLength
        ]);
    }
}
