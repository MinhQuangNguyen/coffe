<?php

namespace App\Services;

use App\Events\Menu\MenuCreated;
use App\Events\Menu\MenuDeleted;
use App\Events\Menu\MenuUpdated;
use App\Models\Menu;
use App\Exceptions\GeneralException;
use App\Services\BaseService;
use Exception;
use Illuminate\Support\Facades\DB;

/**
 * Class MenuService.
 */
class MenuService extends BaseService
{
    /**
     * MenuService constructor.
     *
     * @param  Menu  $menu
     */
    public function __construct(Menu $menu)
    {
        $this->model = $menu;
    }

    /**
     * @return mixed
     */
    public function getParents($id = null)
    {
        $query = $this->model::whereNull('parent_id');
        if($id) $query = $query->where('id', '!=', $id);
        return $query->get();
    }

    /**
     * @param  array  $data
     *
     * @return Menu
     * @throws GeneralException
     * @throws \Throwable
     */
    public function store(array $data = [], $type = 'menu'): Menu
    {
        DB::beginTransaction();
        try {
            $dataT = [
                'sort' => isset($data['sort']) ? $data['sort'] : null,
                'vi' => [
                    'name' => isset($data['name']) ? $data['name'] : null,
                    'link' => isset($data['link']) ? $data['link'] : null,
                    'content' => isset($data['content']) ? $data['content'] : null
                ],
                'en' => [
                    'name' => isset($data['name_en']) ? $data['name_en'] : null,
                    'link' => isset($data['link_en']) ? $data['link_en'] : null,
                    'content' => isset($data['content_en']) ? $data['content_en'] : null
                ]
            ];

            $dataC = [
                'parent_id' => isset($data['parent_id']) ? $data['parent_id'] : null,
                'category_id' => isset($data['category_id']) ? $data['category_id'] : null
            ];

            if($type === 'introduce') {
                $dataC = [
                    'parent_id' => 2,
                    'key' => 'introduce'
                ];
            }

            $menu = $this->model::create(array_merge($dataC, $dataT));
        } catch (Exception $e) {
            DB::rollBack();

            throw new GeneralException(__('There was a problem creating the '.$type.'.'));
        }

        event(new MenuCreated($menu));

        DB::commit();

        return $menu;
    }

    /**
     * @param  Menu  $menu
     * @param  array  $data
     *
     * @return Menu
     * @throws GeneralException
     * @throws \Throwable
     */
    public function update(Menu $menu, array $data = [], $type = 'menu'): Menu
    {
        DB::beginTransaction();
        try {
            $locale = session()->get('locale') ?? env('APP_LANGUAGE');
            $dataT = [
                'sort' => isset($data['sort']) ? $data['sort'] : null,
                $locale => [
                    'name' => isset($data['name']) ? $data['name'] : $menu->name,
                    'link' => isset($data['link']) ? $data['link'] : $menu->link,
                    'content' => isset($data['content']) ? $data['content'] : $menu->content
                ]
            ];

            $dataC = [
                'parent_id' => isset($data['parent_id']) ? $data['parent_id'] : null,
                'category_id' => isset($data['category_id']) ? $data['category_id'] : null
            ];

            if($type === 'introduce') $dataC = [];

            $menu->update(array_merge($dataC, $dataT));
        } catch (Exception $e) {
            DB::rollBack();

            throw new GeneralException(__('There was a problem updating the '.$type.'.'));
        }

        event(new MenuUpdated($menu));

        DB::commit();

        return $menu;
    }

    /**
     * @param  Menu  $menu
     *
     * @return bool
     * @throws GeneralException
     */
    public function destroy(Menu $menu, $type = 'menu'): bool
    {
        if ($this->deleteById($menu->id)) {
            event(new MenuDeleted($menu));

            return true;
        }

        throw new GeneralException(__('There was a problem deleting the '.$type.'.'));
    }

    public function getForHome()
    {
        return $this->model::query()->whereNull('parent_id')->orderBy('sort', 'asc')->get();
    }

    public function getByKey($key, $excludeRoot = true)
    {
        $query = $this->model::query()->where('key', $key);
        if($excludeRoot){
            $query->whereNotNull('parent_id');
        }
        return $query->get();
    }
}
