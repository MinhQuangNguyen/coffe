<?php

namespace App\Events\Recruitment;

use App\Models\Recruitment;
use Illuminate\Queue\SerializesModels;

/**
 * Class RecruitmentUpdated.
 */
class RecruitmentUpdated
{
    use SerializesModels;

    /**
     * @var
     */
    public $recruitment;

    /**
     * @param $recruitment
     */
    public function __construct(Recruitment $recruitment)
    {
        $this->recruitment = $recruitment;
    }
}
