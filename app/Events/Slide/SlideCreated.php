<?php

namespace App\Events\Slide;

use App\Models\Slide;
use Illuminate\Queue\SerializesModels;

/**
 * Class SlideCreated.
 */
class SlideCreated
{
    use SerializesModels;

    /**
     * @var
     */
    public $slide;

    /**
     * @param $slide
     */
    public function __construct(Slide $slide)
    {
        $this->slide = $slide;
    }
}
