<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;

class Recruitment extends Model
{
    use Translatable;
    public $translatedAttributes = ['title', 'slug', 'description', 'content', 'rank', 'form', 'location', 'contact', 'note'];
    public const ACTIVE = 1;
    public const INACTIVE = 0;
    public const FORMS = [
        [ 'key' => 'fulltime', 'value' => 'Full time' ],
        [ 'key' => 'shifts', 'value' => 'Shifts' ]
    ];
    protected $fillable = ['salary_start', 'salary_end', 'career_id', 'address_id', 'degree_id', 'department_id', 'expired', 'active', 'sort'];
    protected $guarded = [];
    protected $casts = [
      'expired'  => 'date:Y-m-d',
    ];

    public function getOptionsAttribute($value)
    {
        return unserialize($value);
    }

    public function address()
    {
        return $this->belongsTo('App\Models\Address');
    }

    public function career()
    {
        return $this->belongsTo('App\Models\Career');
    }

    public function degree()
    {
        return $this->belongsTo('App\Models\Degree');
    }

    public function department()
    {
        return $this->belongsTo('App\Models\Department');
    }

    public function getForm() {
        $forms = array_map(function($itemT) {
            $rs = current(array_filter(self::FORMS, function($item) use ($itemT) {
                return $item['key'] === $itemT;
            }));
            if($rs) return __($rs['value']);
        }, $this->form);
        return implode(", ", $forms);
    }
}
