<section class="ftco-section">
    <div class="container">
        <div class="row">
            <div class="col-md-6 mb-5 pb-3">
                <h3 class="mb-5 heading-pricing wow bounceInLeft" data-wow-delay="0.2s" data-wow-duration="1s">Món phụ</h3>
                <div class="pricing-entry d-flex wow bounceInUp" data-wow-delay="0.3s" data-wow-duration="1s">
                    <div class="img" style="background-image: url(/img/dish-1.jpg);"></div>
                    <div class="desc pl-3">
                        <div class="d-flex text align-items-center">
                            <h3><span>Cornish - Mackerel</span></h3>
                            <span class="price">$20.00</span>
                        </div>
                        <div class="d-block">
                            <p>A small river named Duden flows by their place and supplies</p>
                        </div>
                    </div>
                </div>
                <div class="pricing-entry d-flex wow bounceInUp" data-wow-delay="0.4s" data-wow-duration="1s">
                    <div class="img" style="background-image: url(/img/dish-2.jpg);"></div>
                    <div class="desc pl-3">
                        <div class="d-flex text align-items-center">
                            <h3><span>Roasted Steak</span></h3>
                            <span class="price">$29.00</span>
                        </div>
                        <div class="d-block">
                            <p>A small river named Duden flows by their place and supplies</p>
                        </div>
                    </div>
                </div>
                <div class="pricing-entry d-flex wow bounceInUp" data-wow-delay="0.5s" data-wow-duration="1s">
                    <div class="img" style="background-image: url(/img/dish-3.jpg);"></div>
                    <div class="desc pl-3">
                        <div class="d-flex text align-items-center">
                            <h3><span>Seasonal Soup</span></h3>
                            <span class="price">$20.00</span>
                        </div>
                        <div class="d-block">
                            <p>A small river named Duden flows by their place and supplies</p>
                        </div>
                    </div>
                </div>
                <div class="pricing-entry d-flex wow bounceInUp" data-wow-delay="0.6s" data-wow-duration="1s">
                    <div class="img" style="background-image: url(/img/dish-4.jpg);"></div>
                    <div class="desc pl-3">
                        <div class="d-flex text align-items-center">
                            <h3><span>Chicken Curry</span></h3>
                            <span class="price">$20.00</span>
                        </div>
                        <div class="d-block">
                            <p>A small river named Duden flows by their place and supplies</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 mb-5 pb-3">
                <h3 class="mb-5 heading-pricing wow bounceInRight" data-wow-delay="0.2s" data-wow-duration="1s">Món chính</h3>
                <div class="pricing-entry d-flex wow bounceInUp" data-wow-delay="0.3s" data-wow-duration="1s">
                    <div class="img" style="background-image: url(/img/dish-5.jpg);"></div>
                    <div class="desc pl-3">
                        <div class="d-flex text align-items-center">
                            <h3><span>Sea Trout</span></h3>
                            <span class="price">$49.91</span>
                        </div>
                        <div class="d-block">
                            <p>A small river named Duden flows by their place and supplies</p>
                        </div>
                    </div>
                </div>
                <div class="pricing-entry d-flex wow bounceInUp" data-wow-delay="0.4s" data-wow-duration="1s">
                    <div class="img" style="background-image: url(/img/dish-6.jpg);"></div>
                    <div class="desc pl-3">
                        <div class="d-flex text align-items-center">
                            <h3><span>Roasted Beef</span></h3>
                            <span class="price">$20.00</span>
                        </div>
                        <div class="d-block">
                            <p>A small river named Duden flows by their place and supplies</p>
                        </div>
                    </div>
                </div>
                <div class="pricing-entry d-flex wow bounceInUp" data-wow-delay="0.5s" data-wow-duration="1s">
                    <div class="img" style="background-image: url(/img/dish-7.jpg);"></div>
                    <div class="desc pl-3">
                        <div class="d-flex text align-items-center">
                            <h3><span>Butter Fried Chicken</span></h3>
                            <span class="price">$20.00</span>
                        </div>
                        <div class="d-block">
                            <p>A small river named Duden flows by their place and supplies</p>
                        </div>
                    </div>
                </div>
                <div class="pricing-entry d-flex wow bounceInUp" data-wow-delay="0.6s" data-wow-duration="1s">
                    <div class="img" style="background-image: url(/img/dish-8.jpg);"></div>
                    <div class="desc pl-3">
                        <div class="d-flex text align-items-center">
                            <h3><span>Chiken Filet</span></h3>
                            <span class="price">$20.00</span>
                        </div>
                        <div class="d-block">
                            <p>A small river named Duden flows by their place and supplies</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <h3 class="mb-5 heading-pricing wow bounceInLeft" data-wow-delay="0.2s" data-wow-duration="1s">Tráng miệng</h3>
                <div class="pricing-entry d-flex wow bounceInUp" data-wow-delay="0.3s" data-wow-duration="1s">
                    <div class="img" style="background-image: url(/img/dessert-1.jpg);"></div>
                    <div class="desc pl-3">
                        <div class="d-flex text align-items-center">
                            <h3><span>Cornish - Mackerel</span></h3>
                            <span class="price">$20.00</span>
                        </div>
                        <div class="d-block">
                            <p>A small river named Duden flows by their place and supplies</p>
                        </div>
                    </div>
                </div>
                <div class="pricing-entry d-flex wow bounceInUp" data-wow-delay="0.4s" data-wow-duration="1s">
                    <div class="img" style="background-image: url(/img/dessert-2.jpg);"></div>
                    <div class="desc pl-3">
                        <div class="d-flex text align-items-center">
                            <h3><span>Roasted Steak</span></h3>
                            <span class="price">$29.00</span>
                        </div>
                        <div class="d-block">
                            <p>A small river named Duden flows by their place and supplies</p>
                        </div>
                    </div>
                </div>
                <div class="pricing-entry d-flex wow bounceInUp" data-wow-delay="0.5s" data-wow-duration="1s">
                    <div class="img" style="background-image: url(/img/dessert-3.jpg);"></div>
                    <div class="desc pl-3">
                        <div class="d-flex text align-items-center">
                            <h3><span>Seasonal Soup</span></h3>
                            <span class="price">$20.00</span>
                        </div>
                        <div class="d-block">
                            <p>A small river named Duden flows by their place and supplies</p>
                        </div>
                    </div>
                </div>
                <div class="pricing-entry d-flex wow bounceInUp" data-wow-delay="0.6s" data-wow-duration="1s">
                    <div class="img" style="background-image: url(/img/dessert-4.jpg);"></div>
                    <div class="desc pl-3">
                        <div class="d-flex text align-items-center">
                            <h3><span>Chicken Curry</span></h3>
                            <span class="price">$20.00</span>
                        </div>
                        <div class="d-block">
                            <p>A small river named Duden flows by their place and supplies</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <h3 class="mb-5 heading-pricing wow bounceInRight" data-wow-delay="0.2s" data-wow-duration="1s">Đồ uống</h3>
                <div class="pricing-entry d-flex wow bounceInUp" data-wow-delay="0.3s" data-wow-duration="1s">
                    <div class="img" style="background-image: url(/img/drink-5.jpg);"></div>
                    <div class="desc pl-3">
                        <div class="d-flex text align-items-center">
                            <h3><span>Sea Trout</span></h3>
                            <span class="price">$49.91</span>
                        </div>
                        <div class="d-block">
                            <p>A small river named Duden flows by their place and supplies</p>
                        </div>
                    </div>
                </div>
                <div class="pricing-entry d-flex wow bounceInUp" data-wow-delay="0.4s" data-wow-duration="1s">
                    <div class="img" style="background-image: url(/img/drink-6.jpg);"></div>
                    <div class="desc pl-3">
                        <div class="d-flex text align-items-center">
                            <h3><span>Roasted Beef</span></h3>
                            <span class="price">$20.00</span>
                        </div>
                        <div class="d-block">
                            <p>A small river named Duden flows by their place and supplies</p>
                        </div>
                    </div>
                </div>
                <div class="pricing-entry d-flex wow bounceInUp" data-wow-delay="0.5s" data-wow-duration="1s">
                    <div class="img" style="background-image: url(/img/drink-7.jpg);"></div>
                    <div class="desc pl-3">
                        <div class="d-flex text align-items-center">
                            <h3><span>Butter Fried Chicken</span></h3>
                            <span class="price">$20.00</span>
                        </div>
                        <div class="d-block">
                            <p>A small river named Duden flows by their place and supplies</p>
                        </div>
                    </div>
                </div>
                <div class="pricing-entry d-flex wow bounceInUp" data-wow-delay="0.6s" data-wow-duration="1s">
                    <div class="img" style="background-image: url(/img/drink-8.jpg);"></div>
                    <div class="desc pl-3">
                        <div class="d-flex text align-items-center">
                            <h3><span>Chiken Filet</span></h3>
                            <span class="price">$20.00</span>
                        </div>
                        <div class="d-block">
                            <p>A small river named Duden flows by their place and supplies</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
