@extends('frontend.layouts.coffee')

@section('title', __('Thực đơn'))

@section('content')

    @include('frontend.menufood.includes.topIntro')
    @include('frontend.home.includes.introRestaurent')
    @include('frontend.menufood.includes.listCourse', $projects)
    @include('frontend.home.includes.menuNew')
    {{-- dự án --}}
@endsection
