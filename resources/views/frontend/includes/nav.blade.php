<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar ftco-navbar-light scrolledMenu" id="ftco-navbar">
    <div class="container">
      <a class="navbar-brand" href="/"><img src="/img/logo.png" class="logoDolce"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="oi oi-menu"></span> <img src="/img/menu.svg"/>
      </button>
      <div class="collapse navbar-collapse" id="ftco-nav">
        <ul class="navbar-nav ml-auto">
          @if(isset($menu))
              @foreach ($menu as $menuItem)
                  @if ($menuItem->hasChild())
                            <li class="nav-item dropdown {{ request()->is((strlen($menuItem->link) > 1 ? substr($menuItem->link, 1) : $menuItem->link)) || request()->is((strlen($menuItem->link) > 1 ? substr($menuItem->link, 1) : $menuItem->link)."/*") ? 'active': ''}}">
                                <a class="nav-link" href="{{$menuItem->clickable ? $menuItem->link: '#'}}">
                                    {{ $menuItem->name }}
                                </a>
                            </li>
                        @else
                        <?php $link = strlen($menuItem->link) > 1 ? substr($menuItem->link, 1) : $menuItem->link ?>
                        <li class="nav-item {{ request()->is($link) || request()->is($link.'/*') ? 'active': '' }}">
                            <a class="nav-link" href="{{ $menuItem->link }}">
                                {{ $menuItem->name }}
                            </a>
                        </li>
                        @endif
                @endforeach
            @endif
        </ul>
      </div>
    </div>
</nav>
