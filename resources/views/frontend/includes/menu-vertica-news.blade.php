<div class="menu-vertical">
    <div class="indicator"></div>
    <ul class="menu-list">
        <li class="menu-item active" x-on:click="onChange(0)">
            <a href="javascript:void(0);">@lang('Image')</a>
        </li>
        <li class="menu-item" x-on:click="onChange(1)">
            <a href="javascript:void(0);">@lang('Video')</a>
        </li>
    </ul>
</div>
