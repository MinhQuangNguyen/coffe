<footer class="ftco-footer ftco-section img">
    <div class="overlay"></div>
  <div class="container">
    <div class="row">
      <div class="col-lg-4 col-md-6">
        <div class="ftco-footer-widget mb-4">
          <h2 class="ftco-heading-2 wow bounceInUp" data-wow-delay="0.2s" data-wow-duration="1s">GIỚI THIỆU</h2>
          <p class="wow bounceInUp" data-wow-delay="0.3s" data-wow-duration="1s">Dolce Pub tự hào là một trong những nhà hàng hệ sinh thái tổ hợp, tổ chức sự kiện tại Hà Nội. Chuyên các món Âu và Á, phù hợp cho các thực khách tới trải nghiệm.</p>
          <ul class="ftco-footer-social list-unstyled float-md-left float-lft">
            <li class="">
                <a href="#" class="wow bounceInUp" data-wow-delay="0.4s" data-wow-duration="1s">
                    <img src="/img/twitter.svg" class="icon-social-footter"/>
                </a>
            </li>
            <li class="">
                <a href="#" class="wow bounceInUp" data-wow-delay="0.5s" data-wow-duration="1s">
                    <img src="/img/facebook.svg" class="icon-social-footter"/>
                </a>
            </li>
            <li class="">
                <a href="#" class="wow bounceInUp" data-wow-delay="0.6s" data-wow-duration="1s">
                   <img src="/img/linkedin.svg" class="icon-social-footter"/>
                </a>
            </li>
          </ul>
        </div>
      </div>
      <div class="col-lg-4 col-md-6">
         <div class="ftco-footer-widget mb-4 ml-md-4">
          <h2 class="ftco-heading-2 wow bounceInUp" data-wow-delay="0.2s" data-wow-duration="1s">DỊCH VỤ</h2>
          <div class="row">
            <div class="col-6 col-lg-6">
              <ul class="list-unstyled">
                <li><a href="#" class="py-2 d-block wow bounceInUp" data-wow-delay="0.3s" data-wow-duration="1s">Pha chế rượu</a></li>
                <li><a href="#" class="py-2 d-block wow bounceInUp" data-wow-delay="0.4s" data-wow-duration="1s">Giao hàng</a></li>
                <li><a href="#" class="py-2 d-block wow bounceInUp" data-wow-delay="0.5s" data-wow-duration="1s">Đồ ăn nhanh</a></li>
                <li><a href="#" class="py-2 d-block wow bounceInUp" data-wow-delay="0.6s" data-wow-duration="1s">Đặt cỗ tại nhà</a></li>
              </ul>
            </div>
            <div class="col-6 col-lg-6">
              <ul class="list-unstyled">
                <li><a href="#" class="py-2 d-block wow bounceInUp" data-wow-delay="0.3s" data-wow-duration="1s">Tổ chức đám cưới</a></li>
                <li><a href="#" class="py-2 d-block wow bounceInUp" data-wow-delay="0.4s" data-wow-duration="1s">Tổ chức sinh nhật</a></li>
                <li><a href="#" class="py-2 d-block wow bounceInUp" data-wow-delay="0.5s" data-wow-duration="1s">Tổ chức sự kiện</a></li>
                <li><a href="#" class="py-2 d-block wow bounceInUp" data-wow-delay="0.6s" data-wow-duration="1s">Hỗ trợ đặt bàn</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6">
        <div class="ftco-footer-widget mb-4">
            <h2 class="ftco-heading-2 wow bounceInUp" data-wow-delay="0.2s" data-wow-duration="1s">LIÊN HỆ</h2>
            <div class="block-23 mb-3">
              <ul>
                <li class="wow bounceInUp" data-wow-delay="0.3s" data-wow-duration="1s"><a href=""><span class="fa fa-map-marker" aria-hidden="true"></span><span class="text">{!! $setting['address'] !!}</span></a></li>
                <li class="wow bounceInUp" data-wow-delay="0.4s" data-wow-duration="1s"><a href="tel:0345736211"><span class="fa fa-phone" aria-hidden="true"></span><span class="text">{!! $setting['tel'] !!}</span></a></li>
                <li class="wow bounceInUp" data-wow-delay="0.5s" data-wow-duration="1s"><a href="mail:hotro@mediaiconic.com"><span class="fa fa-envelope" aria-hidden="true"></span><span class="text">{!! $setting['email'] !!}</span></a></li>
              </ul>
            </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 text-center wow bounceInLeft" data-wow-delay="0.4s" data-wow-duration="1s">
        <p class="coypyrightMqn">&copy;Bản quyền thuộc về Media Iconic</a></p>
      </div>
    </div>
  </div>
</footer>

<div class="wrapper">
    <div class="ring">
        <a href="tel:0345736211">
          <div class="coccoc-alo-phone coccoc-alo-green coccoc-alo-show">
              <div class="coccoc-alo-ph-circle"></div>
              <div class="coccoc-alo-ph-circle-fill"></div>
              <div class="coccoc-alo-ph-img-circle"></div>
          </div>
        </a>
    </div>
</div>