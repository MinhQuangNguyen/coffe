@extends('frontend.layouts.coffee')

@section('title', __('Contact'))

@section('content')
@include('frontend.contact.includes.top')
@include('frontend.contact.includes.contactForm')
@include('frontend.contact.includes.map')
@endsection
