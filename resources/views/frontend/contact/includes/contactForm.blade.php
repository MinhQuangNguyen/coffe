<section class="ftco-section contact-section">
    <div class="container mt-5">
        <div class="row block-9">
            <div class="col-md-4 contact-info heading-section">
                <div class="row">
                    <div class="col-md-12 mb-4 wow bounceInLeft" data-wow-delay="0.2s" data-wow-duration="1s">
                        <h2 class="h4">Thông tin liên hệ</h2>
                    </div>
                    <div class="col-md-12 mb-3 inherit wow bounceInLeft" data-wow-delay="0.3s" data-wow-duration="1s">
                        <p><span>Địa chỉ: </span><a href="">{!! $setting['address'] !!}</a></p>
                    </div>
                    <div class="col-md-12 mb-3 inherit wow bounceInLeft" data-wow-delay="0.4s" data-wow-duration="1s">
                        <p><span>Số điện thoại: </span> <a href="tel://0345736211">{!! $setting['tel'] !!}</a></p>
                    </div>
                    <div class="col-md-12 mb-3 inherit wow bounceInLeft" data-wow-delay="0.5s" data-wow-duration="1s">
                        <p><span>Email: </span><a href="mailto:hotro@mediaiconic.com">{!! $setting['email'] !!}</a></p>
                    </div>
                    <div class="col-md-12 mb-3 inherit wow bounceInLeft" data-wow-delay="0.6s" data-wow-duration="1s">
                        <p><span>Website: </span><a href="#">coffemon.mediaiconic.com</a></p>
                    </div>
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-6">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                            <strong>{{ $message }}</strong>
                    </div>
                @endif
                <x-forms.post :action="route('frontend.contact.store')">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group wow bounceInRight" data-wow-delay="0.2s" data-wow-duration="1s">
                                <input type="text" name="name" class="form-control" placeholder="Họ và tên" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group wow bounceInRight" data-wow-delay="0.3s" data-wow-duration="1s">
                                <input type="text" class="form-control" name="email" placeholder="Địa chỉ email" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group wow bounceInRight" data-wow-delay="0.4s" data-wow-duration="1s">
                        <input type="text" class="form-control" name="phone" placeholder="Số điện thoại" required>
                    </div>
                    <div class="form-group wow bounceInRight" data-wow-delay="0.5s" data-wow-duration="1s">
                        <textarea name="content" id="" cols="30" rows="7" class="form-control" placeholder="Lời nhắn" required></textarea>
                    </div>
                    <div class="form-group wow bounceInRight" data-wow-delay="0.6s" data-wow-duration="1s">
                        <input type="submit" value="Gửi tin nhắn" class="btn btn-primary py-3 px-5">
                    </div>
                </x-forms.post>
            </div>
        </div>
    </div>
</section>

