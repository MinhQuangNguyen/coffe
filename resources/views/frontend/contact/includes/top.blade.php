<section>
    <div class="slider-item-mqn" style="">
        <div class="overlay"></div>
        <div class="container">
            <div class="row slider-text justify-content-center align-items-center pt-15">
                <div class="col-md-7 col-sm-12 text-center heading-section">
                    <span class="subheading wow bounceInRight" data-wow-delay="0.2s" data-wow-duration="1s">Contact</span>
                    <h2 class="bread wow bounceInLeft" data-wow-delay="0.3s" data-wow-duration="1s">Liên hệ</h2>
                    <p class="breadcrumbs wow bounceInRight" data-wow-delay="0.4s" data-wow-duration="1s">
                        <span class="mr-2"><a href="index.html">Trang chủ</a></span>
                        <span>Liên hệ</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
