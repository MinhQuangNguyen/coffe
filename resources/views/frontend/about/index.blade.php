@extends('frontend.layouts.coffee')

@section('title', __('Giới thiệu'))

@section('content')

@include('frontend.about.includes.top')
@include('frontend.home.includes.brand')
@include('frontend.home.includes.customer')
@include('frontend.home.includes.menu')
@include('frontend.home.includes.achievements')

@endsection
