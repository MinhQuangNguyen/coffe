 <section>
      <div class="slider-item-mqn" style="">
      	<div class="overlay"></div>
        <div class="container">
          <div class="row slider-text justify-content-center align-items-center pt-15">
            <div class="col-md-7 col-sm-12 text-center">
                <span class="subheading">Blog</span>
            	<h1 class="bread">Tin mới</h1>
	            <p class="breadcrumbs"><span><a href="index.html">Tin tức</a></span> <span>{!! $post->title !!}</span></p>
            </div>
          </div>
        </div>
      </div>
    </section>


<section class="ftco-section">
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <h2 class="mb-3">{!! $post->title !!}</h2>
            <div class="detail-blog-content-mqn">
              {!! $post->content !!}
            </div>
          </div> <!-- .col-md-8 -->
          <div class="col-md-4 sidebar">
            <div class="sidebar-box">
              <div class="categories">
                <h3>Danh mục</h3>
                <li><a href="#">Du lịch <span>(12)</span></a></li>
                <li><a href="#">Khách sạn <span>(22)</span></a></li>
                <li><a href="#">Vui chơi<span>(37)</span></a></li>
                <li><a href="#">Đồ uống <span>(42)</span></a></li>
                <li><a href="#">Đồ ăn <span>(14)</span></a></li>
                <li><a href="#">Tiệc tùng <span>(140)</span></a></li>
              </div>
            </div>

            <div class="sidebar-box">
              <h3>TIN TỨC</h3>
                @if(isset($news_relates))
                  @foreach($news_relates as $key => $item)
                  <div class="block-21 mb-4 d-flex">
                    <a class="blog-img mr-4" style="background-image: url({{ asset( $item->image) }});"></a>
                    <div class="text">
                      <h3 class="heading"><a href="{{ $item->category->getLink().'/'.$item->category->slug.'/'.$item->slug.'.html' }}">{{ $item->title }}</a></h3>
                      <div class="meta">
                        <div><a href="#"><span class="icon-calendar"></span> {{ date_format($item->created_at, 'd/m/Y') }}</a></div>
                        <div><a href="#"><span class="icon-person"></span> {{ $item->author }}</a></div>
                      </div>
                    </div>
                  </div>
                  @endforeach
                @endif
            </div>

            <div class="sidebar-box">
              <h3>Từ khóa</h3>
              <div class="tagcloud">
                <a href="#" class="tag-cloud-link">Văn Hóa</a>
                <a href="#" class="tag-cloud-link">Ẩm thực</a>
                <a href="#" class="tag-cloud-link">Lễ hội</a>
                <a href="#" class="tag-cloud-link">Diệu kỳ</a>
                <a href="#" class="tag-cloud-link">Ăn ngon</a>
                <a href="#" class="tag-cloud-link">Ngọt</a>
                <a href="#" class="tag-cloud-link">Tráng miệng</a>
                <a href="#" class="tag-cloud-link">Đồ ngon</a>
              </div>
            </div>
        </div>
      </div>
    </section> <!-- .section -->