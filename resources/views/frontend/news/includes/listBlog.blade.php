<section class="ftco-section">
    <div class="container">
        <div class="row d-flex">
            @foreach($posts as $post)
            <div class="col-md-4 d-flex">
                <div class="blog-entry">
                    <a href="/tin-tuc/{{ $post->category->slug.'/'.$post->slug.'.html' }}" class="block-20"
                        style="background-image: url('{{ asset($post->image) }}');">
                    </a>
                    <div class="text py-4 d-block">
                        <div class="meta">
                            <div><a
                                    href="/tin-tuc/{{ $post->category->slug.'/'.$post->slug.'.html' }}">{{ date_format($post->created_at, 'd/m/Y') }}</a>
                            </div>
                            <div><a
                                    href="/tin-tuc/{{ $post->category->slug.'/'.$post->slug.'.html' }}">{{ $post->author }}</a>
                            </div>
                        </div>
                        <h3 class="heading mt-2"><a href="#">{{ $post->title }}</a></h3>
                        <p>{{ $post->description_short }}</p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="row mt-5">
            <div class="col text-center">
                {{ $posts->appends(request()->query())->links() }}
            </div>
        </div>
    </div>
</section>
