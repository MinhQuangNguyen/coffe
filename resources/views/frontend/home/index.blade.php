@extends('frontend.layouts.coffee')

@section('title', __('Home'))

@section('content')
@include('frontend.home.includes.slide')
@include('frontend.home.includes.introRestaurent')
@include('frontend.home.includes.brand')
@include('frontend.home.includes.service')
@include('frontend.home.includes.menu')
@include('frontend.home.includes.achievements')
@include('frontend.home.includes.bestseller')
@include('frontend.home.includes.gallery')
@include('frontend.home.includes.menuNew')
@include('frontend.home.includes.customer')
@include('frontend.home.includes.new')
@include('frontend.home.includes.map')

@endsection
