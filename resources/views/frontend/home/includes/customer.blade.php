
<section class="ftco-section img" id="ftco-testimony" style="background-image: url(/img/bg_1.jpg);"  data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-7 heading-section text-center">
              <span class="subheading wow bounceInUp" data-wow-delay="0.2s" data-wow-duration="1s">Customer</span>
            <h2 class="mb-4 wow bounceInLeft" data-wow-delay="0.3s" data-wow-duration="1s">Trải nghiệm thực khách</h2>
            <p class="wow bounceInRight" data-wow-delay="0.4s" data-wow-duration="1s">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
          </div>
        </div>
      </div>
      <div class="container-wrap">
        <div class="row d-flex no-gutters">
          <div class="col-lg align-self-sm-end wow bounceInUp" data-wow-delay="0.5s" data-wow-duration="1s">
            <div class="testimony">
               <blockquote>
                  <p>&ldquo;Tôi rất hài lòng khi thực được trải nghiệm ẩm thực tại nhà hàng cũng như văn hóa không gian tại nhà hàng. Cảm giác mới lạ và khác biệt&rdquo;</p>
                </blockquote>
                <div class="author d-flex mt-4">
                  <div class="image mr-3 align-self-center">
                    <img src="/img/person_2.jpg" alt="">
                  </div>
                  <div class="name align-self-center">Minh Quang Nguyen <span class="position">Thực khách</span></div>
                </div>
            </div>
          </div>
          <div class="col-lg align-self-sm-end wow bounceInUp" data-wow-delay="0.6s" data-wow-duration="1s">
            <div class="testimony overlay">
               <blockquote>
                  <p>&ldquo;Đồ ăn và thức uống chuẩn bị Á Âu, hương vị mới lạ trong từng món ăn. Tôi cảm thấy rất hài lòng khi trải nghiệm nhà hàng cũng như cách phục vụ.&rdquo;</p>
                </blockquote>
                <div class="author d-flex mt-4">
                  <div class="image mr-3 align-self-center">
                    <img src="/img/person_2.jpg" alt="">
                  </div>
                  <div class="name align-self-center">Vũ Thị Hồng Ngọc <span class="position">Thực khách</span></div>
                </div>
            </div>
          </div>
          <div class="col-lg align-self-sm-end wow bounceInUp" data-wow-delay="0.7s" data-wow-duration="1s">
            <div class="testimony">
               <blockquote>
                  <p>&ldquo;Đồ ăn ngon, giá hợp lý, có chỗ để ô tô và không gian quán rất tây. Một nơi hẹn hò lý tưởng cho các cắp đôi. &rdquo;</p>
                </blockquote>
                <div class="author d-flex mt-4">
                  <div class="image mr-3 align-self-center">
                    <img src="/img/person_3.jpg" alt="">
                  </div>
                  <div class="name align-self-center">Hoàng Anh Minh <span class="position">Thực khách</span></div>
                </div>
            </div>
          </div>
          <div class="col-lg align-self-sm-end wow bounceInUp" data-wow-delay="0.8s" data-wow-duration="1s">
            <div class="testimony overlay">
               <blockquote>
                  <p>&ldquo;Phục vụ và tư vấn nhiệt tình, tuy chưa trải nghiệm tại nhà hàng nhưng đồ ăn giao hàng rất nhanh lại còn được free ship.&rdquo;</p>
                </blockquote>
                <div class="author d-flex mt-4">
                  <div class="image mr-3 align-self-center">
                    <img src="/img/person_2.jpg" alt="">
                  </div>
                  <div class="name align-self-center">Hoàng Thùy <span class="position">Thực khách</span></div>
                </div>
            </div>
          </div>
          <div class="col-lg align-self-sm-end wow bounceInUp" data-wow-delay="0.9s" data-wow-duration="1s">
            <div class="testimony">
              <blockquote>
                <p>&ldquo;Đồ ăn ngon, phục vụ được cả Âu lẫn Á. Tổ chức sự kiện tại nhà hàng rất hoàng tráng, lần tới tôi sẽ lại quay lại nhà hàng để tổ chức. &rdquo;</p>
              </blockquote>
              <div class="author d-flex mt-4">
                <div class="image mr-3 align-self-center">
                  <img src="/img/person_3.jpg" alt="">
                </div>
                <div class="name align-self-center">Nguyễn Đức Vượng <span class="position">Thực khách</span></div>
              </div>
            </div>
          </div>
        </div>
      </div>
</section>