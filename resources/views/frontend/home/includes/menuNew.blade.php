
  <section class="ftco-menu">
      <div class="container">
          <div class="row justify-content-center mb-5">
        <div class="col-md-7 heading-section text-center">
            <span class="subheading wow bounceInUp" data-wow-delay="0.2s" data-wow-duration="1s">Menu New</span>
          <h2 class="mb-4 wow bounceInLeft" data-wow-delay="0.3s" data-wow-duration="1s">Hôm nay có gì?</h2>
          <p class="wow bounceInRight" data-wow-delay="0.4s" data-wow-duration="1s">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
        </div>
      </div>
          <div class="row d-md-flex">
              <div class="col-lg-12 p-md-5">
                  <div class="row">
                <div class="col-md-12 nav-link-wrap mb-5 wow bounceInUp" data-wow-delay="0.5s" data-wow-duration="1s">
                  <div class="nav nav-pills justify-content-center" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link active" id="v-pills-1-tab" data-toggle="pill" href="#v-pills-1" role="tab" aria-controls="v-pills-1" aria-selected="true">Món chính</a>

                    <a class="nav-link" id="v-pills-2-tab" data-toggle="pill" href="#v-pills-2" role="tab" aria-controls="v-pills-2" aria-selected="false">Đồ uống</a>

                    <a class="nav-link" id="v-pills-3-tab" data-toggle="pill" href="#v-pills-3" role="tab" aria-controls="v-pills-3" aria-selected="false">Tráng miệng</a>
                  </div>
                </div>
                <div class="col-md-12 d-flex align-items-center">
                  
                  <div class="tab-content" id="v-pills-tabContent">

                    <div class="tab-pane fade show active" id="v-pills-1" role="tabpanel" aria-labelledby="v-pills-1-tab">
                        <div class="row">
                            <div class="col-md-4 text-center wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1s">
                                <div class="menu-wrap">
                                    <a href="#" class="menu-img img mb-4" style="background-image: url(/img/dish-1.jpg);"></a>
                                    <div class="text">
                                        <h3><a href="#">beefsteak</a></h3>
                                        <p>Bít tết, là một món ăn bao gồm miếng thịt bò lát phẳng, thường được nướng vỉ, áp chảo hoặc nướng broiling ở nhiệt độ cao.</p>
                                        <p class="price"><span>$5.90</span></p>
                                        <p><a href="#" class="btn btn-primary btn-outline-primary">Đặt ngay</a></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 text-center wow bounceInUp" data-wow-delay="0.2s" data-wow-duration="1s">
                                <div class="menu-wrap">
                                    <a href="#" class="menu-img img mb-4" style="background-image: url(/img/dish-2.jpg);"></a>
                                    <div class="text">
                                        <h3><a href="#">Sườn nướng BBQ</a></h3>
                                        <p>Sườn nướng BBQ là muốn ăn Âu, được thực hiện bởi đầu bếp Minh Quang Nguyen, Hàng đầu thế giới hiện nay. Mang lại cảm giác lạ</p>
                                        <p class="price"><span>$3.30</span></p>
                                        <p><a href="#" class="btn btn-primary btn-outline-primary">Đặt ngay</a></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 text-center wow bounceInUp" data-wow-delay="0.3s" data-wow-duration="1s">
                                <div class="menu-wrap">
                                    <a href="#" class="menu-img img mb-4" style="background-image: url(/img/dish-3.jpg);"></a>
                                    <div class="text">
                                        <h3><a href="#">Grilled Beef</a></h3>
                                        <p>Grilled Beef là sự đột phá trong cách nấu ăn, mang sự mới mẻ trong beef cho thực khách trải nghiệm tại nhà hàng</p>
                                        <p class="price"><span>$6.20</span></p>
                                        <p><a href="#" class="btn btn-primary btn-outline-primary">Đặt ngay</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="v-pills-2" role="tabpanel" aria-labelledby="v-pills-2-tab">
                      <div class="row">
                            <div class="col-md-4 text-center wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1s">
                                <div class="menu-wrap">
                                    <a href="#" class="menu-img img mb-4" style="background-image: url(/img/drink-1.jpg);"></a>
                                    <div class="text">
                                        <h3><a href="#">Lemonade Juice</a></h3>
                                        <p>Cam tươi thêm một chút chanh, mang lại cảm giác mới mẻ trong đồ uống dành cho mua hè.</p>
                                        <p class="price"><span>$3.50</span></p>
                                        <p><a href="#" class="btn btn-primary btn-outline-primary">Đặt ngay</a></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 text-center wow bounceInUp" data-wow-delay="0.2s" data-wow-duration="1s">
                                <div class="menu-wrap">
                                    <a href="#" class="menu-img img mb-4" style="background-image: url(/img/drink-2.jpg);"></a>
                                    <div class="text">
                                        <h3><a href="#">Vang đỏ</a></h3>
                                        <p>Vang đỏ tại nhà hàng được trưng cất trên 20 năm tuổi. Mang lại hơi ấm và độ đầm đà nhất định</p>
                                        <p class="price"><span>$8.28</span></p>
                                        <p><a href="#" class="btn btn-primary btn-outline-primary">Đặt ngay</a></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 text-center wow bounceInUp" data-wow-delay="0.3s" data-wow-duration="1s">
                                <div class="menu-wrap">
                                    <a href="#" class="menu-img img mb-4" style="background-image: url(/img/drink-3.jpg);"></a>
                                    <div class="text">
                                        <h3><a href="#">Soda Chanh</a></h3>
                                        <p>Đồ uống không thể thiếu trong những bữa nướng, giúp đánh tan vị ngấy trong những miếng thịt.</p>
                                        <p class="price"><span>$1.90</span></p>
                                        <p><a href="#" class="btn btn-primary btn-outline-primary">Đặt ngay</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="v-pills-3" role="tabpanel" aria-labelledby="v-pills-3-tab">
                      <div class="row">
                            <div class="col-md-4 text-center wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1s">
                                <div class="menu-wrap">
                                    <a href="#" class="menu-img img mb-4" style="background-image: url(/img/dessert-1.jpg);"></a>
                                    <div class="text">
                                        <h3><a href="#">Bánh mật ong</a></h3>
                                        <p>Được chế biến bởi mật ong rừng tây bắc, thêm một lớp kem tươi vị dâu, giúp cho món ăn khác lạ và độc đáo</p>
                                        <p class="price"><span>$3.20</span></p>
                                        <p><a href="#" class="btn btn-primary btn-outline-primary">Đặt ngay</a></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 text-center wow bounceInUp" data-wow-delay="0.2s" data-wow-duration="1s">
                                <div class="menu-wrap">
                                    <a href="#" class="menu-img img mb-4" style="background-image: url(/img/dessert-2.jpg);"></a>
                                    <div class="text">
                                        <h3><a href="#">Bánh ngọt việt quất</a></h3>
                                        <p>Việt quất mọng nước được chế biến thành hương liệu cho bánh, mang lại cảm giác thơm ngọt nhẹ.</p>
                                        <p class="price"><span>$2.90</span></p>
                                        <p><a href="#" class="btn btn-primary btn-outline-primary">Đặt ngay</a></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 text-center wow bounceInUp" data-wow-delay="0.3s" data-wow-duration="1s">
                                <div class="menu-wrap">
                                    <a href="#" class="menu-img img mb-4" style="background-image: url(/img/dessert-3.jpg);"></a>
                                    <div class="text">
                                        <h3><a href="#">Bánh kem vị dâu</a></h3>
                                        <p>Bánh dâu không còn gì xa lạ với thực khách, nhưng nhàng đã cho thêm 1 chút hương liệu đặc biệt, làm món ăn trở lên khác lạ</p>
                                        <p class="price"><span>$5.32</span></p>
                                        <p><a href="#" class="btn btn-primary btn-outline-primary">Đặt ngay</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
  </section>