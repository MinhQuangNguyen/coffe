  <section class="ftco-section">
    <div class="container">
      <div class="row justify-content-center mb-5 pb-3">
        <div class="col-md-7 heading-section text-center">
        <span class="subheading wow bounceInUp" data-wow-delay="0.2s" data-wow-duration="1s">Blog New</span>
          <h2 class="mb-4 wow bounceInLeft" data-wow-delay="0.3s" data-wow-duration="1s">Tin tức mới nhất</h2>
          <p class="wow bounceInRight" data-wow-delay="0.4s" data-wow-duration="1s">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
        </div>
      </div>
      <div class="row d-flex">
       @foreach($posts as $post) 
        <div class="col-md-4 d-flex wow bounceInUp" data-wow-delay="0.5s" data-wow-duration="1s">
            <div class="blog-entry align-self-stretch">
            <a href="/tin-tuc/{{ $post->category->slug.'/'.$post->slug.'.html' }}" class="block-20" style="background-image: url('{{ asset($post->image) }}');">
            </a>
            <div class="text py-4 d-block">
                <div class="meta">
                <div><a href="/tin-tuc/{{ $post->category->slug.'/'.$post->slug.'.html' }}">{{ date_format($post->created_at, 'd/m/Y') }}</a></div>
                <div><a href="/tin-tuc/{{ $post->category->slug.'/'.$post->slug.'.html' }}">{{ $post->author }}</a></div>
              </div>
              <h3 class="heading mt-2"><a href="/tin-tuc/{{ $post->category->slug.'/'.$post->slug.'.html' }}">{{ $post->title }}</a></h3>
              <p>{{ $post->description_short }}</p>
            </div>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </section>