  <section class="ftco-section">
      <div class="container">
          <div class="row justify-content-center mb-5 pb-3">
        <div class="col-md-7 heading-section text-center">
            <span class="subheading wow bounceInUp" data-wow-delay="0.2s" data-wow-duration="1s">Best Seller</span>
          <h2 class="mb-4 wow bounceInLeft" data-wow-delay="0.3s" data-wow-duration="1s">Đồ uống bán chạy</h2>
          <p class="wow bounceInRight" data-wow-delay="0.4s" data-wow-duration="1s">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
        </div>
      </div>
      <div class="row">
          <div class="col-md-3 wow bounceInUp" data-wow-delay="0.2s" data-wow-duration="1s">
              <div class="menu-entry">
                      <a href="#" class="img" style="background-image: url(/img/menu-1.jpg);"></a>
                      <div class="text text-center pt-4">
                          <h3><a href="#">Coffee Capuccino</a></h3>
                          <p>Thêm 1 chút vị ngọt và ấm từ cà phê. Mang lại cảm giác gợi nhớ mùa thu.</p>
                          <p class="price"><span>$5.90</span></p>
                          <p><a href="#" class="btn btn-primary btn-outline-primary">Đặt ngay</a></p>
                      </div>
                  </div>
          </div>
          <div class="col-md-3 wow bounceInUp" data-wow-delay="0.3s" data-wow-duration="1s">
              <div class="menu-entry">
                      <a href="#" class="img" style="background-image: url(/img/menu-2.jpg);"></a>
                      <div class="text text-center pt-4">
                          <h3><a href="#">Đen đường không đá</a></h3>
                          <p>Cảm giác rất lạ khi sử dụng nóng, mang lại cảm giác ngọt nhẹ đầu môi.</p>
                          <p class="price"><span>$5.90</span></p>
                          <p><a href="#" class="btn btn-primary btn-outline-primary">Đặt ngay</a></p>
                      </div>
                  </div>
          </div>
          <div class="col-md-3 wow bounceInUp" data-wow-delay="0.4s" data-wow-duration="1s">
              <div class="menu-entry">
                      <a href="#" class="img" style="background-image: url(/img/menu-3.jpg);"></a>
                      <div class="text text-center pt-4">
                          <h3><a href="#">Cà phê mật</a></h3>
                          <p>Cái đắng của cà phê thêm chút ngọt mật ong rừng. Mang lại cảm giác lạ</p>
                          <p class="price"><span>$5.90</span></p>
                          <p><a href="#" class="btn btn-primary btn-outline-primary">Đặt ngay</a></p>
                      </div>
                  </div>
          </div>
          <div class="col-md-3 wow bounceInUp" data-wow-delay="0.5s" data-wow-duration="1s">
              <div class="menu-entry">
                      <a href="#" class="img" style="background-image: url(/img/menu-4.jpg);"></a>
                      <div class="text text-center pt-4">
                          <h3><a href="#">Bạc sỉu</a></h3>
                          <p>Không còn gì xa lạ, Hương vị sẽ ấm và ngọt hơn. Gợi nhớ kỉ niệm.</p>
                          <p class="price"><span>$5.90</span></p>
                          <p><a href="#" class="btn btn-primary btn-outline-primary">Đặt ngay</a></p>
                      </div>
                  </div>
          </div>
      </div>
      </div>
  </section>
