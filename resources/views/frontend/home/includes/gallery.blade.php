  <section class="ftco-gallery">
      <div class="container-wrap">
          <div class="row no-gutters">
                  <div class="col-md-3 wow bounceInUp" data-wow-delay="0.2s" data-wow-duration="1s">
                      <span href="" class="gallery img d-flex align-items-center" style="background-image: url(/img/gallery-1.jpg);">
                         
                      </span>
                  </div>
                  <div class="col-md-3 wow bounceInUp" data-wow-delay="0.3s" data-wow-duration="1s">
                      <span href="" class="gallery img d-flex align-items-center" style="background-image: url(/img/gallery-2.jpg);">
                         
                      </span>
                  </div>
                  <div class="col-md-3 wow bounceInUp" data-wow-delay="0.4s" data-wow-duration="1s">
                      <span href="" class="gallery img d-flex align-items-center" style="background-image: url(/img/gallery-3.jpg);">
                         
                      </span>
                  </div>
                  <div class="col-md-3 wow bounceInUp" data-wow-delay="0.5s" data-wow-duration="1s">
                      <span href="" class="gallery img d-flex align-items-center" style="background-image: url(/img/gallery-4.jpg);">
                         
                      </span>
                  </div>
      </div>
      </div>
  </section>