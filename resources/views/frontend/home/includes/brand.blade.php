<section class="ftco-about d-md-flex">
    <div class="one-half img wow pulse" data-wow-delay="0.2s" data-wow-duration="1s" style="background-image: url(/img/about.jpg);"></div>
    <div class="one-half">
        <div class="overlap wow pulse" data-wow-delay="0.2s" data-wow-duration="1s">
            <div class="heading-section">
                <span class="subheading wow bounceInRight" data-wow-delay="0.2s" data-wow-duration="1s">Dolce Pub</span>
                <h2 class="mb-4 wow bounceInRight" data-wow-delay="0.3s" data-wow-duration="1s">Khu tổ hợp sự kiện</h2>
            </div>
            <div class="wow bounceInRight" data-wow-delay="0.4s" data-wow-duration="1s">
                <p>Dolce Pub là quán pub ở Hà Nội đặc biệt thú vị. Không gian trẻ trung, năng động và đầy tinh tế. Đa dạng các thể loại nhạc được chơi để bạn tha hồ chill cùng đám bạn. Đặc biệt nhất, những li cocktail đầy sáng tạo sẽ khiến bạn say đắm. Dàn bartender đầy tâm huyết tạo ra những li cocktail hấp dẫn với hương vị thơm ngon. Đến ngay Dolce Pub để trải nghiệm phong cách phục vụ chuyên nghiệp cùng âm nhạc sôi động nhé.</p>
            </div>
        </div>
    </div>
</section>
