<section class="ftco-intro">
    <div class="container-wrap">
        <div class="wrap d-md-flex align-items-xl-end">
            <div class="info wow pulse" data-wow-delay="0.1s" data-wow-duration="1s">
                <div class="row no-gutters">
                    <div class="col-md-4 d-flex wow bounceInUp" data-wow-delay="0.2s" data-wow-duration="1s">
                        <div class="icon"><span class="fa fa-phone"></span></div>
                        <div class="text">
                            <h3>Hỗ trợ đặt bàn 24/7</h3>
                            {!! $setting['tel'] !!}
                        </div>
                    </div>
                    <div class="col-md-4 d-flex wow bounceInUp" data-wow-delay="0.4s" data-wow-duration="1s">
                        <div class="icon"><span class="fa fa-map-marker"></span></div>
                        <div class="text">
                            <h3>Khu tổ hợp sinh thái</h3>
                            {!! $setting['address'] !!}
                        </div>
                    </div>
                    <div class="col-md-4 d-flex wow bounceInUp" data-wow-delay="0.6s" data-wow-duration="1s">
                        <div class="icon"><span class="fa fa-clock"></span></div>
                        <div class="text">
                            <h3>Mở các ngày trong tuần</h3>
                            {!! $setting['open'] !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="book p-4 wow pulse" data-wow-delay="0.1s" data-wow-duration="1s">
                <h3 class="wow bounceInRight" data-wow-delay="0.2s" data-wow-duration="1s">Hãy liên hệ với tôi để đặt
                    bàn</h3>
                <form action="#" method="post" class="appointment-form">
                    <div class="d-md-flex">
                        <div class="form-group wow bounceInRight" data-wow-delay="0.3s" data-wow-duration="1s">
                            <input type="text" class="form-control" placeholder="Họ" required>
                        </div>
                        <div class="form-group ml-md-4 wow bounceInRight" data-wow-delay="0.4s" data-wow-duration="1s">
                            <input type="text" class="form-control" placeholder="Tên" required>
                        </div>
                    </div>
                    <div class="d-md-flex">
                        <div class="form-group wow bounceInRight" data-wow-delay="0.5s" data-wow-duration="1s">
                            <div class="input-wrap">
                                <div class="icon"><span class="ion-md-calendar"></span></div>
                                <input type="date" id="dateid" class="form-control appointment_date" placeholder="Ngày" required>
                            </div>
                        </div>
                        <div class="form-group ml-md-4 wow bounceInRight" data-wow-delay="0.6s" data-wow-duration="1s">
                            <div class="input-wrap">
                                <div class="icon"><span class="ion-ios-clock"></span></div>
                                <input type="time" id="theTime" class="form-control appointment_time" placeholder="Giờ" required>
                            </div>
                        </div>
                        <div class="form-group ml-md-4 wow bounceInRight" data-wow-delay="0.7s" data-wow-duration="1s">
                            <input type="text" class="form-control" placeholder="Số điện thoại" required>
                        </div>
                    </div>
                    <div class="d-md-flex">
                        <div class="form-group wow bounceInRight" data-wow-delay="0.8s" data-wow-duration="1s">
                            <textarea name="" id="" cols="30" rows="2" class="form-control"
                                placeholder="Lời nhắn"></textarea>
                        </div>
                        <div class="form-group ml-md-4 wow bounceInRight" data-wow-delay="0.9s" data-wow-duration="1s">
                            <input type="submit" value="Đặt bàn ngay" class="btn btn-white py-3 px-4">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
