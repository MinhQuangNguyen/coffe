
  <section class="ftco-section ftco-service">
      <div class="container">
          <div class="row">
        <div class="col-md-4 wow bounceInUp" data-wow-delay="0.2s" data-wow-duration="1s">
          <div class="media d-block text-center block-6 DỊCH VỤ">
            <div class="icon d-flex justify-content-center align-items-center mb-5">
                <img src="/img/choices.svg" class="img-service-mqn"/>
            </div>
            <div class="media-body">
              <h3 class="heading">Thanh toán dễ dàng</h3>
              <p>Nhà hàng cung cấp tất cả các dịch vụ thanh toán như chuyển khoản, quét thẻ hoặc tiền mặt.</p>
            </div>
          </div>      
        </div>
        <div class="col-md-4 wow bounceInUp" data-wow-delay="0.3s" data-wow-duration="1s">
          <div class="media d-block text-center block-6 DỊCH VỤ">
            <div class="icon d-flex justify-content-center align-items-center mb-5">
               <img src="/img/delivery-truck.svg" class="img-service-mqn"/>
            </div>
            <div class="media-body">
              <h3 class="heading">Miễn phí vận chuyển</h3>
              <p>Nhà hàng miễn phí giao hàng với các đơn trên 1 triệu và dưới 5km.</p>
            </div>
          </div>      
        </div>
        <div class="col-md-4 wow bounceInUp" data-wow-delay="0.4s" data-wow-duration="1s">
          <div class="media d-block text-center block-6 DỊCH VỤ">
            <div class="icon d-flex justify-content-center align-items-center mb-5">
               <img src="/img/support.svg" class="img-service-mqn"/>
            </div>
            <div class="media-body">
              <h3 class="heading">Tư vấn nhiệt tình</h3>
              <p>Tổ tư vấn nhà hàng hoạt động 24/7, giúp thực khách giải đáp mọi thắc mắc.</p>
            </div>
          </div>    
        </div>
      </div>
      </div>
  </section>
