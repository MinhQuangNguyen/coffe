<section class="header-top-mqn">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>

        <div class="carousel-inner">
          @foreach($slides as $key => $slide)
            <div class="carousel-item {{ $key === 0 ? 'active' : ''}}">
                <img class="d-block w-100" src="{{ asset($slide->image) }}" alt="First slide">
                <div class="carousel-caption">
                    <span class="subheading wow bounceInDown" data-wow-delay="0.2s"
                        data-wow-duration="1s">Welcome</span>
                    <div class="heading-section">
                        <h2 class="wow bounceInLeft" data-wow-delay="0.3s" data-wow-duration="1s">{{ $slide->title }}</h2>
                    </div>
                    <p class="mb-md-5 wow bounceInRight" data-wow-delay="0.4s" data-wow-duration="1s">{!! $slide->content !!}</p>
                    <p><a href="/thuc-don" class="btn btn-primary p-3 px-xl-4 py-xl-3 wow bounceInLeft"
                            data-wow-delay="0.5s" data-wow-duration="1s">Đặt hàng ngay</a> <a href="/thuc-don"
                            class="btn btn-white btn-outline-white p-3 px-xl-4 py-xl-3 wow bounceInRight"
                            data-wow-delay="0.5s" data-wow-duration="1s">Xem thực đơn</a></p>
                </div>
            </div>
          @endforeach
        </div>

        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>
