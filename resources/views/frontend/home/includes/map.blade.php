<section class="ftco-appointment">
    <div class="overlay"></div>
    <div class="container-wrap">
        <div class="row no-gutters d-md-flex align-items-center">
            <div class="col-md-6 d-flex align-self-stretch wow bounceInLeft" data-wow-delay="0.2s" data-wow-duration="1s">
                <iframe width="100%" height="500" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
                    src="https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=en&amp;q=12%20Khu%E1%BA%A5t%20Duy%20Ti%E1%BA%BFn+(My%20Business%20Name)&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"></iframe>
            </div>
            <div class="col-md-6 appointment wow bounceInRight">
                <h3 class="mb-3 wow bounceInRight" data-wow-delay="0.2s" data-wow-duration="1s">Hãy liên hệ với tôi để đặt bàn</h3>
                <form action="#" method="post" class="appointment-form">
                    <div class="d-md-flex">
                        <div class="form-group wow bounceInRight" data-wow-delay="0.3s" data-wow-duration="1s">
                            <input type="text" class="form-control" placeholder="Họ" required>
                        </div>
                        <div class="form-group ml-md-4 wow bounceInRight" data-wow-delay="0.4s" data-wow-duration="1s">
                            <input type="text" class="form-control" placeholder="Tên" required>
                        </div>
                    </div>
                    <div class="d-md-flex">
                        <div class="form-group wow bounceInRight" data-wow-delay="0.5s" data-wow-duration="1s">
                            <div class="input-wrap">
                                <div class="icon"><span class="ion-md-calendar"></span></div>
                                <input type="date" id="dateid2" class="form-control appointment_date" placeholder="Ngày" required>
                            </div>
                        </div>
                        <div class="form-group ml-md-4 wow bounceInRight" data-wow-delay="0.6s" data-wow-duration="1s">
                            <div class="input-wrap">
                                <div class="icon"><span class="ion-ios-clock"></span></div>
                                <input type="time" id="theTime2" class="form-control appointment_time" placeholder="Giờ" required>
                            </div>
                        </div>
                        <div class="form-group ml-md-4 wow bounceInRight" data-wow-delay="0.7s" data-wow-duration="1s">
                            <input type="text" class="form-control" placeholder="Số điện thoại" required>
                        </div>
                    </div>
                    <div class="d-md-flex">
                        <div class="form-group wow bounceInRight" data-wow-delay="0.8s" data-wow-duration="1s">
                            <textarea name="" id="" cols="30" rows="2" class="form-control" placeholder="Lời nhắn"
                                required></textarea>
                        </div>
                        <div class="form-group ml-md-4 wow bounceInRight" data-wow-delay="0.9s" data-wow-duration="1s">
                            <input type="submit" value="Đặt bàn ngay" class="btn btn-primary py-3 px-4">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
