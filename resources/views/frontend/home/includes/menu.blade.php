  <section class="ftco-section">
      <div class="container">
          <div class="row align-items-center">
              <div class="col-md-6 pr-md-5">
                  <div class="heading-section text-md-right">
                <span class="subheading wow bounceInLeft" data-wow-delay="0.2s" data-wow-duration="1s">Best Drink</span>
              <h2 class="mb-4 wow bounceInLeft" data-wow-delay="0.3s" data-wow-duration="1s">Thế giới đồ uống</h2>
              <p class="mb-4 wow bounceInLeft" data-wow-delay="0.4s" data-wow-duration="1s">Tại Dolce Pub không chỉ có những cốc cocktail mang lại cảm giác mới lạ, mà còn những cốc cà phê độc đáo mang lại trải nghiệm không thể quên bởi thực khách, hưa hẹn mang lại nhiều trải nghiệm khác lạ cho thực khách.</p>
              <p><a href="#" class="btn btn-primary btn-outline-primary px-4 py-3 wow bounceInLeft" data-wow-delay="0.5s" data-wow-duration="1s">Xem toàn bộ thực đơn</a></p>
            </div>
              </div>
              <div class="col-md-6">
                  <div class="row">
                      <div class="col-md-6">
                          <div class="menu-entry wow bounceInUp" data-wow-delay="0.2s" data-wow-duration="1s">
                              <span href="#" class="img" style="background-image: url(/img/menu-1.jpg);"></span>
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="menu-entry mt-lg-4 wow bounceInRight" data-wow-delay="0.3s" data-wow-duration="1s">
                              <span href="#" class="img" style="background-image: url(/img/menu-2.jpg);"></span>
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="menu-entry wow bounceInLeft" data-wow-delay="0.4s" data-wow-duration="1s">
                              <span href="#" class="img" style="background-image: url(/img/menu-3.jpg);"></span>
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="menu-entry mt-lg-4 wow bounceInDown" data-wow-delay="0.5s" data-wow-duration="1s">
                              <span href="#" class="img" style="background-image: url(/img/menu-4.jpg);"></span>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>
