
  <section class="ftco-counter ftco-bg-dark img" id="section-counter" style="background-image: url(/img/bg_2.jpg);" data-stellar-background-ratio="0.5">
          <div class="overlay"></div>
    <div class="container">
      <div class="row justify-content-center">
          <div class="col-md-10">
              <div class="row">
                <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap wow bounceInUp" data-wow-delay="0.2s" data-wow-duration="1s">
                  <div class="block-18 text-center">
                    <div class="text">
                        <div class="icon">
                            <img src="/img/coffee-cup.svg" class="img-coffe-cup"/>
                        </div>
                        <strong class="number" data-number="100">100</strong>
                        <span>Chi nhánh cà phê</span>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap wow bounceInUp" data-wow-delay="0.3s" data-wow-duration="1s">
                  <div class="block-18 text-center">
                    <div class="text">
                        <div class="icon">
                            <img src="/img/coffee-cup.svg" class="img-coffe-cup"/>
                        </div>
                        <strong class="number" data-number="85">85</strong>
                        <span>Giải thưởng</span>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap wow bounceInUp" data-wow-delay="0.4s" data-wow-duration="1s">
                  <div class="block-18 text-center">
                    <div class="text">
                        <div class="icon">
                            <img src="/img/coffee-cup.svg" class="img-coffe-cup"/>
                        </div>
                        <strong class="number" data-number="10567">10567</strong>
                        <span>Khách hàng hài lòng</span>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap wow bounceInUp" data-wow-delay="0.5s" data-wow-duration="1s">
                  <div class="block-18 text-center">
                    <div class="text">
                        <div class="icon">
                           <img src="/img/coffee-cup.svg" class="img-coffe-cup"/>
                        </div>
                        <strong class="number" data-number="900">900</strong>
                        <span>Nhân viên</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
      </div>
    </div>
  </section>

