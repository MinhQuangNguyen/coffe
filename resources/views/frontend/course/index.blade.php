@extends('frontend.layouts.coffee')

@section('title', __('Course'))

@section('content')
@include('frontend.course.includes.listCourse')
@endsection
