@extends('frontend.layouts.coffee')

@section('title', __('Field'))

@section('content')

@include('frontend.field.includes.slide', ['slide' => $field])

@include('frontend.field.includes.tabsMenuField')

<div class="tab-content" id="nav-tabContent">
    @if(isset($fields))
    @foreach($fields as $key => $field)
    <div class="tab-pane fade show {{ $key === 0 ? 'active' : '' }}"
        id="{{ 'nav-'.$field->key }}" role="tabpanel"
        aria-labelledby="{{ 'tab-'.$field->key }}"
    >
        <div class="item-project-location">
            <div class="background-img-overview pb-h-field" style="height : {{ $field->key === 'finance' ? '100%' : 'calc(100% - 170px)' }}"></div>
            <div class="container">
                <div class="item-body-shadow page-overview" style="margin-bottom : {{ $field->key === 'finance' ? '146px' : '40px' }}">
                    <div class="item-info-project-location">
                        <div class="title">@lang('Overview')</div>
                        <div class="text pb-text">{{ $field->description }}</div>
                    </div>
                </div>

                <?php $projects = $field->getProjects() ?>

                @if($field->key !== 'finance')
                @include('frontend.field.includes.project', [ 'projects' => $projects, 'future' => $future ])
                @endif

                <!-- <div class="item-next-page-project" style="background-image: url('{{ asset('img/img9.png') }}');">
                    <div class="item-info-next-page">
                        <div class="info-next-page">
                            <div class="title">@lang('You are interested in the project')</div>
                            <div class="text">{{ count($projects) ? $projects[0]->name.'?' : 'Feni City Phú Quốc?' }}</div>
                            <div class="click-page-project">
                                <x-utils.link :href="app()->getLocale() === 'vi' ? '/du-an' : '/project'"
                                    :text="__('Go to project page')" class="readmore readmore-red" />
                            </div>
                            <div class="text-hotline">@lang('Or contact hotline')</div>
                            <div class="hotline">{!! $setting['hotline'] !!}</div>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
    @endforeach
    @endif
</div>
@endsection
