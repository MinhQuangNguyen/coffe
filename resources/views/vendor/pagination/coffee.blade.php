@if ($paginator->hasPages())
    <div class="block-27">
        <ul>
            @if ($paginator->onFirstPage())
                 <li><a href="{{ $paginator->previousPageUrl() }}"><img src="/img/back.svg" class="back"/></a></li>
            @else
                <li><a href="{{ $paginator->previousPageUrl() }}"><img src="/img/back.svg" class="back"/></a></li>
             @endif

            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="disabled" aria-disabled="true"><span>{{ $element }}</span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="active" aria-current="page"><span>{{ $page }}</span></li>
                        @else
                            <li><a href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            @if ($paginator->hasMorePages())
                <li><a href="{{ $paginator->nextPageUrl() }}"><img src="/img/next.svg" class="next"/></a></li>
            @else
                <li><a href="{{ $paginator->nextPageUrl() }}"><img src="/img/next.svg" class="next"/></a></li>
            @endif
        </ul>
    </div>
@endif




