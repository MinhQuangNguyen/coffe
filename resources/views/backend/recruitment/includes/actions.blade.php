<x-utils.edit-button :href="route('admin.recruitment.edit', $recruitment)" />
<x-utils.delete-button :href="route('admin.recruitment.destroy', $recruitment)" />
