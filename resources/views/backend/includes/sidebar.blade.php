<div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show" id="sidebar">
    <div class="c-sidebar-brand d-lg-down-none">
        <img src="/img/logo.svg" alt="hinhanh" width="160px">
    </div><!--c-sidebar-brand-->

    <ul class="c-sidebar-nav">
        <li class="c-sidebar-nav-item">
            <x-utils.link
                class="c-sidebar-nav-link"
                :href="route('admin.dashboard')"
                :active="activeClass(Route::is('admin.dashboard'), 'c-active')"
                icon="c-sidebar-nav-icon cil-speedometer"
                :text="__('Dashboard')"
            />
        </li>

        @if ($logged_in_user->hasAllAccess() || ($logged_in_user->can('admin.access.introduce.list')))
        <li class="c-sidebar-nav-item">
            <x-utils.link
                class="c-sidebar-nav-link"
                :href="route('admin.introduce.index')"
                :active="activeClass(Route::is('admin.introduce.*'), 'c-active')"
                icon="c-sidebar-nav-icon cil-info"
                :text="__('Introduce Management')"
            />
        </li>
        @endif

        @if ($logged_in_user->hasAllAccess() || ($logged_in_user->can('admin.access.field.list')))
        <li class="c-sidebar-nav-item">
            <x-utils.link
                class="c-sidebar-nav-link"
                :href="route('admin.field.index')"
                :active="activeClass(Route::is('admin.field.*'), 'c-active')"
                icon="c-sidebar-nav-icon cil-terrain"
                :text="__('Field Management')"
            />
        </li>
        @endif

        @if ($logged_in_user->hasAllAccess() || ($logged_in_user->can('admin.access.project.list')))
        <li class="c-sidebar-nav-item">
            <x-utils.link
                class="c-sidebar-nav-link"
                :href="route('admin.project.index')"
                :active="activeClass(Route::is('admin.project.*'), 'c-active')"
                icon="c-sidebar-nav-icon cil-spreadsheet"
                :text="__('Project Management')"
            />
        </li>
        @endif

        @if ($logged_in_user->hasAllAccess() || ($logged_in_user->can('admin.access.category.list')))
        <li class="c-sidebar-nav-item">
            <x-utils.link
                class="c-sidebar-nav-link"
                :href="route('admin.category.index')"
                :active="activeClass(Route::is('admin.category.*'), 'c-active')"
                icon="c-sidebar-nav-icon cil-apps"
                :text="__('Category Management')"
            />
        </li>
        @endif

        @if ($logged_in_user->hasAllAccess() || ($logged_in_user->can('admin.access.post.list')))
        <li class="c-sidebar-nav-item">
            <x-utils.link
                class="c-sidebar-nav-link"
                :href="route('admin.post.index')"
                :active="activeClass(Route::is('admin.post.*'), 'c-active')"
                icon="c-sidebar-nav-icon cil-newspaper"
                :text="__('Post Management')"
            />
        </li>
        @endif

        @if ($logged_in_user->hasAllAccess() || ($logged_in_user->can('admin.access.banner.list')))
        <li class="c-sidebar-nav-item">
            <x-utils.link
                class="c-sidebar-nav-link"
                :href="route('admin.banner.index')"
                :active="activeClass(Route::is('admin.banner.*'), 'c-active')"
                icon="c-sidebar-nav-icon cil-tv"
                :text="__('Banner Management')"
            />
        </li>
        @endif

        @if ($logged_in_user->hasAllAccess() || ($logged_in_user->can('admin.access.recruitment.list')))
        <li class="c-sidebar-nav-item">
            <x-utils.link
                class="c-sidebar-nav-link"
                :href="route('admin.recruitment.index')"
                :active="activeClass(Route::is('admin.recruitment.*'), 'c-active')"
                icon="c-sidebar-nav-icon cil-briefcase"
                :text="__('Recruitment Management')"
            />
        </li>
        @endif

        @if ($logged_in_user->hasAllAccess() || ($logged_in_user->can('admin.access.contact.list')))
        <li class="c-sidebar-nav-item">
            <x-utils.link
                class="c-sidebar-nav-link"
                :href="route('admin.contact.index')"
                :active="activeClass(Route::is('admin.contact.*'), 'c-active')"
                icon="c-sidebar-nav-icon cil-contact"
                :text="__('Contact Management')"
            />
        </li>
        @endif

        @if ($logged_in_user->hasAllAccess() || ($logged_in_user->can('admin.access.setting.list')))
        <li class="c-sidebar-nav-item">
            <x-utils.link
                class="c-sidebar-nav-link"
                :href="route('admin.setting.index')"
                :active="activeClass(Route::is('admin.setting.*'), 'c-active')"
                icon="c-sidebar-nav-icon cil-settings"
                :text="__('Setting')"
            />
        </li>
        @endif

        @if (
            $logged_in_user->hasAllAccess() ||
            (
                $logged_in_user->can('admin.access.user.list') ||
                $logged_in_user->can('admin.access.user.deactivate') ||
                $logged_in_user->can('admin.access.user.reactivate') ||
                $logged_in_user->can('admin.access.user.clear-session') ||
                $logged_in_user->can('admin.access.user.impersonate') ||
                $logged_in_user->can('admin.access.user.change-password')
            )
        )
            <li class="c-sidebar-nav-title">@lang('System')</li>

            @if ($logged_in_user->hasAllAccess() || ($logged_in_user->can('admin.access.menu.list')))
            <li class="c-sidebar-nav-item">
                <x-utils.link
                    class="c-sidebar-nav-link"
                    :href="route('admin.menu.index')"
                    :active="activeClass(Route::is('admin.menu.*'), 'c-active')"
                    icon="c-sidebar-nav-icon cil-hamburger-menu"
                    :text="__('Menu Management')"
                />
            </li>
            @endif

            <li class="c-sidebar-nav-dropdown {{ activeClass(Route::is('admin.auth.user.*') || Route::is('admin.auth.role.*'), 'c-open c-show') }}">
                <x-utils.link
                    href="#"
                    icon="c-sidebar-nav-icon cil-user"
                    class="c-sidebar-nav-dropdown-toggle"
                    :text="__('Access')" />

                <ul class="c-sidebar-nav-dropdown-items">
                    @if (
                        $logged_in_user->hasAllAccess() ||
                        (
                            $logged_in_user->can('admin.access.user.list') ||
                            $logged_in_user->can('admin.access.user.deactivate') ||
                            $logged_in_user->can('admin.access.user.reactivate') ||
                            $logged_in_user->can('admin.access.user.clear-session') ||
                            $logged_in_user->can('admin.access.user.impersonate') ||
                            $logged_in_user->can('admin.access.user.change-password')
                        )
                    )
                        <li class="c-sidebar-nav-item">
                            <x-utils.link
                                :href="route('admin.auth.user.index')"
                                class="c-sidebar-nav-link"
                                :text="__('User Management')"
                                :active="activeClass(Route::is('admin.auth.user.*'), 'c-active')" />
                        </li>
                    @endif

                    @if ($logged_in_user->hasAllAccess())
                        <li class="c-sidebar-nav-item">
                            <x-utils.link
                                :href="route('admin.auth.role.index')"
                                class="c-sidebar-nav-link"
                                :text="__('Role Management')"
                                :active="activeClass(Route::is('admin.auth.role.*'), 'c-active')" />
                        </li>
                    @endif
                </ul>
            </li>
        @endif

        @if ($logged_in_user->hasAllAccess())
            <li class="c-sidebar-nav-dropdown">
                <x-utils.link
                    href="#"
                    icon="c-sidebar-nav-icon cil-list"
                    class="c-sidebar-nav-dropdown-toggle"
                    :text="__('Logs')" />

                <ul class="c-sidebar-nav-dropdown-items">
                    <li class="c-sidebar-nav-item">
                        <x-utils.link
                            :href="route('log-viewer::dashboard')"
                            class="c-sidebar-nav-link"
                            :text="__('Dashboard')" />
                    </li>
                    <li class="c-sidebar-nav-item">
                        <x-utils.link
                            :href="route('log-viewer::logs.list')"
                            class="c-sidebar-nav-link"
                            :text="__('Logs')" />
                    </li>
                </ul>
            </li>
        @endif
    </ul>

    <button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent" data-class="c-sidebar-minimized"></button>
</div><!--sidebar-->
