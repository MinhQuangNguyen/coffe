<x-utils.edit-button :href="route('admin.menu.edit', $menu)" />
<x-utils.delete-button :href="route('admin.menu.destroy', $menu)" />
