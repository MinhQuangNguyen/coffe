const animateCSS = require('./animate')
var check = false

function init() {
  $(".menu-vertical .active").click()
}

function zoomImg() {
    if(!check) {
        mediumZoom('.container .content-8.active .item-info-banner-chart img', { background: '#ffffff' })
        check = true
    }
}

function menuVertical() {
  $(".menu-vertical").off("click").on("click", ".menu-item", function () {
    const $parent = $(this).parents(".menu-vertical");
    //kiểm tra đang chạy animation không
    if ($parent.attr("data-changing") == "1") return;

    $parent.find(".active").removeClass("active")
    $(this).addClass("active");
    const $indicator = $parent.find(".indicator");
    const menuItemHeight = $(this).height();
    const newTop = $(this).position().top + (menuItemHeight - $indicator.height()) / 2
    /* anime({
      targets: ".indicator",
      translateY: newTop,
      duration: 1000,
      easing: 'easeInOutExpo'
      //easing: [0.68, -0.55, 0.265, 1.55]
    }); */
    $indicator.css({
      transform: `translateY(${newTop}px)`
    })
    //update content
    const id = $(this).data("id");
    const $content = $(".menu-vertical-content")
    if ($content.length) {
      const $oldElement = $content.find(".active");
      const $newElement = $content.find(".content-" + id)
      $content.css({ minHeight: $newElement.height() })
      animateCSS($content.find(".animate__fadeIn").removeClass("active"), "fadeOut")
      //animateCSS($content.find(".animate__fadeIn").removeClass("active"), "fadeOut")
      animateCSS($newElement.addClass("active"), $oldElement.index() < $newElement.index() ? "fadeIn" : "fadeIn").then(() => {
      })
      if($newElement.hasClass('content-8')) zoomImg()
    }

    //custom for about page
    const newTitle = $(this).data("title");
    const navActive = $('.nav-item.dropdown.active')
    if (newTitle) {
      navActive.find('.active').removeClass('active')
      navActive.find(`[data-href='${$(this).data("url")}']`).addClass('active')
      document.title = newTitle;
      window.history.pushState('', newTitle, $(this).data("url"));
    }

  })
}

$("document").ready(function () {
  menuVertical();
  setTimeout(function () {
    init();
  }, 500)
})
