//header menu change background
$(window).scroll(function() {
    $('nav').toggleClass('scrolledMenu', $(this).scrollTop() > 150);
});
// End header menu change background


// Input date
var date = new Date();

var day = ("0" + date.getDate()).slice(-2);
var month = ("0" + (date.getMonth() + 1)).slice(-2);

var today = date.getFullYear() + "-" + (month) + "-" + (day);

$('#dateid').val(today);
$('#dateid2').val(today);
// End Input date


// Input time
$(document).ready(function(){    
 
    setInterval(updateTime,1000);
    
    function updateTime(){
        //create date instance to get now time 
        var date = new Date();
        hours = date.getHours();
        minutes = date.getMinutes();
        seconds = date.getSeconds();
        
        //add extra zero if the minutes is less than 2 digits
        if(hours < 10){
          hours = '0' + hours;
        }

        //add extra zero if the minutes is less than 2 digits
        if(minutes < 10){
          minutes = '0' + minutes;
        }
      
        $('input[type="time"]').each(function(){
            $(this).attr({'value': hours + ':' + minutes + ':' + seconds});
        });
      
      //document.getElementById("theTime").value = hours+":"+minutes+":"+seconds;
      
    }
 
});
// End Input time

