import 'alpinejs'

window.$ = window.jQuery = require('jquery');
window.Swal = require('sweetalert2');
window.Dropzone = require('dropzone');

require('@coreui/coreui');

// CoreUI
require('@coreui/coreui');

// Boilerplate
require('../plugins');
require('bootstrap-datepicker');
require('select2');
