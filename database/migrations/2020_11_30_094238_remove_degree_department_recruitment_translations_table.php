<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveDegreeDepartmentRecruitmentTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recruitment_translations', function (Blueprint $table) {
            $table->dropColumn('degree');
            $table->dropColumn('department');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recruitment_translations', function (Blueprint $table) {
            $table->string('degree')->nullable();
            $table->string('department')->nullable();
        });
    }
}
